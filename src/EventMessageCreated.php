<?php

namespace Insolutions\Conversations;

use Illuminate\Queue\SerializesModels;

class EventMessageCreated
{
    use SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @param  Message  $message
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }
}