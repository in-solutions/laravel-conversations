<?php

namespace Insolutions\Conversations;
 
use Illuminate\Http\Request;

use \Insolutions\Files\File;

use Auth;

class Controller extends \App\Http\Controllers\Controller
{
    
	public function getConversations(Request $r) {
		return response()->json(
			Conversation::with('participants.user')
				->whereHas('participants.user', function($q)
					{
					    $q->where('id', '=', Auth::user() ? Auth::user()->id : NULL);

					})
				->orderBy('updated_at', 'desc')
				->paginate($r->perPage ?: 20)
		);
	}

	public function getConversation(Request $r, $conversation_id) {
		return response()->json(
			Conversation::with('participants.user')->findOrFail($conversation_id)
		);
	}

	public function getConversationMessages(Request $r, $conversation_id) {
		$itemsPerPage = $r->perPage ?: 50;		
		$q = Conversation::findOrFail($conversation_id)
				->messages()->with('files');

		if ($r->afterMessage) {
			// filter only messages newer than given message_id
			$q->where('id', '>', $r->afterMessage);

			// to return all messages newer, but keep response structure (pagination), when limit causes problems, it should be solved logically
			$itemsPerPage = 1000; 

		} elseif ($r->beforeMessage) {
			// filter only messages older than given message_id
			$q->where('id', '<', $r->beforeMessage);
		}

		return response()->json(
			$q->orderBy('created_at','desc')->paginate($itemsPerPage)
		);
	}

	public function postMessage(Request $r, $conversation_id) {
		// will return message model associated with 
		$msg = Conversation::findOrFail($conversation_id)
			->createMessage($r->text);

		if ($r->attach_files) {
			foreach ($r->attach_files as $fileId) {
				$fileModel = File::findOrFail($fileId);
				$msg->files()->save($fileModel);
			}
		}

		$msg->files; // to load files relationships

		return response()->json($msg);
	}

}