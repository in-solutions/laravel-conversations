<?php

namespace Insolutions\Conversations;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use Auth;

class Conversation extends Model
{
    use SoftDeletes;

	protected $table = 't_conversation';

	protected $fillable = [
		'title'
	];

	protected $hidden = [
		'deleted_at'
    ];

	protected $appends = [
		'last_message'
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function createMessage($text) {
		$msg = new Message;
		$msg->text = $text;
		$msg->conversation()->associate($this);
		$msg->user()->associate(Auth::user());
		$msg->save();

		event(new EventMessageCreated($msg));

		return $msg;
	}

	public function getLastMessageAttribute() {
		return $this->messages()->with('files')->orderBy('created_at', 'desc')->first();
	}

	public function messages() {
		return $this->hasMany('Insolutions\Conversations\Message');
	}

	public function participants() {
		return $this->hasMany('Insolutions\Conversations\Participant');
	}

	public function addUser(User $u) {
		$p = new  Participant;
		$p->user()->associate($u);
		$p->conversation()->associate($this);
		return $p->save();
	} 

	public function scopeWhereUserIsParticipant($query, User $user) {
		return $query->whereHas('participants.user', function ($q) use ($user) {
				$q->where('id', '=', $user->id);
			});
	}
}
