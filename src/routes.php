<?php

Route::group(['prefix' => 'conversations'], function () {
	
	Route::get('conversation', 'Insolutions\Conversations\Controller@getConversations');
	Route::get('conversation/{conversation_id}', 'Insolutions\Conversations\Controller@getConversation');
	Route::get('conversation/{conversation_id}/message', 
		'Insolutions\Conversations\Controller@getConversationMessages');

	Route::post('conversation/{conversation_id}/message',
		'Insolutions\Conversations\Controller@postMessage');

});