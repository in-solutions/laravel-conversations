<?php

namespace Insolutions\Conversations;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;

class Participant extends Model
{
    use SoftDeletes;
	
	protected $table = 't_conversation_participant';
	
	// update updated_at of conversation, when updated/created
	protected $touches = ['conversation'];

	protected $fillable = [	
	];

	protected $hidden = [
		'deleted_at',
		'conversation_id',
		'user_id',
		'id'
    ];

	protected $appends = [
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function conversation() {
		return $this->belongsTo('Insolutions\Conversations\Conversation');
	}
}
