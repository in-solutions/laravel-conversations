<?php

namespace Insolutions\Conversations;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{	
	protected $table = 't_conversation_file';
	
	// update updated_at of conversation, when updated/created
	protected $touches = ['conversation'];

	public $timestamps = null;

	public function message() {
		return $this->belongsTo('Insolutions\Conversations\Message');
	}

	public function file() {
		return $this->belongsTo('Insolutions\Files\File');
	}
}
