<?php

namespace Insolutions\Conversations;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;

class Message extends Model
{
    use SoftDeletes;
	
	protected $table = 't_conversation_message';
	
	// update updated_at of conversation, when updated/created
	protected $touches = ['conversation'];

	protected $fillable = [
		'text'	
	];

	protected $hidden = [
		'deleted_at',
		'conversation_id'
    ];

	protected $appends = [
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function conversation() {
		return $this->belongsTo('Insolutions\Conversations\Conversation');
	}

	public function files() {
		return $this->belongsToMany('Insolutions\Files\File', 't_conversation_file', 'message_id', 'file_id');
	}
}
