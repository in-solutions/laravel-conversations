
CREATE TABLE IF NOT EXISTS `t_conversation_file` (
`id` bigint(20) unsigned NOT NULL,
  `message_id` bigint(20) unsigned NOT NULL,
  `file_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `t_conversation_file`
 ADD PRIMARY KEY (`id`), ADD KEY `message_id` (`message_id`), ADD KEY `file_id` (`file_id`);


ALTER TABLE `t_conversation_file`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `t_conversation_file`
ADD CONSTRAINT `t_conversation_file_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `t_conversation_message` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `t_conversation_file_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `t_file` (`id`) ON UPDATE CASCADE;
