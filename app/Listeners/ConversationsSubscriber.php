<?php

namespace App\Listeners;

class ConversationsSubscriber
{
    /**
     * Handle user login events.
     */
    public function onMessageCreated($event) {
        // project specific actions when new message is created
        // $event->message contains Insolutions/Conversations/Message model
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
             'Insolutions\Conversations\EventMessageCreated',
             'App\Listeners\ConversationsSubscriber@onMessageCreated'
        );

    }

}